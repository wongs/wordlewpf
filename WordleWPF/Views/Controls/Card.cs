﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WordleWPF.Views.Controls
{
    public class Card : Control
    {
        #region Fields

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text),
            typeof(string),
            typeof(Card),
            new PropertyMetadata());

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            nameof(CornerRadius),
            typeof(CornerRadius),
            typeof(Card),
            new PropertyMetadata());

        #endregion

        #region Constructors

        static Card()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(Card),
                new FrameworkPropertyMetadata(typeof(Card)));
        }

        #endregion

        #region Properties

        public string Text
        {
            get => (string)this.GetValue(TextProperty);
            set => this.SetValue(TextProperty, value);
        }

        public CornerRadius CornerRadius
        {
            get => (CornerRadius)this.GetValue(CornerRadiusProperty);
            set => this.SetValue(CornerRadiusProperty, value);
        }

        #endregion
    }
}
