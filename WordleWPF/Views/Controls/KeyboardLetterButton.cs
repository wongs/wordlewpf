﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WordleWPF.Models.Enums;

namespace WordleWPF.Views.Controls
{
    internal class KeyboardLetterButton : Control
    {

        #region Fields

        public static readonly DependencyProperty StateProperty = DependencyProperty.Register(
            nameof(State),
            typeof(LetterState),
            typeof(KeyboardLetterButton),
            new FrameworkPropertyMetadata(LetterState.None));

        #endregion

        #region Constructors

        static KeyboardLetterButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(KeyboardLetterButton),
                new FrameworkPropertyMetadata(typeof(KeyboardLetterButton)));
        }

        #endregion

        #region Properties

        public LetterState State
        {
            get => (LetterState)this.GetValue(StateProperty);
            set => this.SetValue(StateProperty, value);
        }

        #endregion
    }
}
