﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordleWPF.Models.Enums
{
    public enum LetterState
    {
        None,
        RightLetterRightPlace,
        RightLetterWrongPlace,
        WrongLetter
    }
}
