﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordleWPF.Models
{
    public static class Constants
    {

        #region Fields

        public const int MaximumGuesses = 6;

        public const int TimeBetweenReveals = 300;

        #endregion
    }
}
