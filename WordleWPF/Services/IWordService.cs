﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordleWPF.Services
{
    public interface IWordService
    {
        public Task<string> GetDailyWordAsync();

        public Task<bool> IsRecognizedWordAsync(string word);
    }
}
